package chapter04

fun invokeSomeStuff(lambda:()->Unit) {
    lambda()
}

fun main(args: Array<String>) {
    invokeSomeStuff({
        println("doSomeStuff called");
    })
}