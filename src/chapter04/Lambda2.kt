package chapter04


fun main(args: Array<String>) {
    val sum = { x: Int, y: Int -> x + y }
    println("Sum ${sum(10,13)}")
    println("Sum ${sum(52,68)}")
}