package chapter01

/*
// Introducimos una nueva clase Biscuit que se ve casi igual a Cupcake
class Biscuit(val flavour: String) {
  fun eat(): String {
    return "nom, nom, nom... delicious $flavour biscuit"
  }
}*/

// Podemos recatorizar estas clases creando la clase BakeryGood marcado como open para que se puede heredar
/*open class BakeryGood(val flavour: String) {
  fun eat(): String {
    return "nom, nom, nom... delicious $flavour bakery good"
  }
}

// Declaramos las clases extendiendo de BakeryGood en una relacion es-un o es-una
class Cupcake(flavour: String): BakeryGood(flavour)
class Biscuit(flavour: String): BakeryGood(flavour)*/

open class BakeryGood(val flavour: String) {
    fun eat(): String {
        return "nom, nom, nom... delicious $flavour ${name()}"
    }

    open fun name(): String {
        return "bakery good"
    }
}

// Sobre escribimos el metodo name de la clase BakeryGood para que regrese el nombre de cupcake
class Cupcake(flavour: String) : BakeryGood(flavour) {
    override fun name(): String {
        return "cupcake"
    }
}

// Lo mismo hacemos en la clase Biscuit
class Biscuit(flavour: String) : BakeryGood(flavour) {
    override fun name(): String {
        return "biscuit"
    }
}


/*
fun main(args: Array<String>) {
    val myBlueberryCupcake: BakeryGood = Cupcake("Blueberry")

    // La salida es: nom, nom, nom... delicious Blueberry cupcake
    println(myBlueberryCupcake.eat())
}*/

// En este ejemplo la subclase Roll puede ser extendida tambien solo deben ser marcadas como open
open class Roll(flavour: String) : BakeryGood(flavour) {
    override fun name(): String {
        return "roll"
    }
}

// Subclase de BakeryGood que puede ser extendida
open class Donut(flavour: String, val topping: String) : BakeryGood(flavour) {
    override fun name(): String {
        return "donut with $topping topping"
    }
}

fun main(args: Array<String>) {
    val myDonut = Donut("Custard", "Powdered sugar")
    println(myDonut.eat())
}