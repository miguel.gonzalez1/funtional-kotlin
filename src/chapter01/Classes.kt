package chapter01

/* Declaracion de la clase
class VeryBasic

// Metodo principal
fun main(args: Array<String>) {

    // Una forma de declarar la clase de forma explicita, ya que basic es del tipo VeryBasic
    val basic: VeryBasic = VeryBasic()
}*/

/*
fun main(args: Array<String>) {

    // Otra forma de declarar una clase pero implicitamente ya que Kotlin puede inferir el tipo de clase
    val basic = VeryBasic()
}*/


/*
class BlueberryCupcake {

    // Las clases pueden tener estados y en kotlin son representados por las propiedades
    var flavour = "Blueberry"
}*/

/*
fun main(args: Array<String>) {

    // Ya que se declaro una propiedad como variable (var), su valor puede cambiar en ejecucion
    val myCupcake = BlueberryCupcake()
    myCupcake.flavour = "Almond"
    println("My cupcake has ${myCupcake.flavour}")
}*/

/*
class BlueberryCupcake {

    // En la vida real el sabor de un pastel no cambia, y para eso declaramos el flavour como value (val)
    val flavour = "Blueberry"
}

fun main(args: Array<String>) {
    val myCupcake = BlueberryCupcake()
    myCupcake.flavour = "Almond" //Error de compilacion: Val no puede ser reasignado
    println("My cupcake has ${myCupcake.flavour}")
}*/

/*
// Declaramos otra clase nueva con una propiedad flavour como value
class AlmondCupcake {
    val flavour = "Almond"
}

fun main(args: Array<String>) {
    val mySecondCupcake = AlmondCupcake()
    println("My second cupcake has ${mySecondCupcake.flavour} flavour")
}*/

// En la vida real no tienes diferentes recipientes para cocinar cada pastel, es por eso que se define una clase Cupcake
// con la propiedad flavour como value.
/*class Cupcake(flavour: String) {
    val flavour = flavour
}*/

/*
// Ademas se definio un constructor con un parametro flavour
class Cupcake(val flavour: String)

// Ahora podemos definir varias instancias con diferentes valores
fun main(args: Array<String>) {
    val myBlueberryCupcake = Cupcake("Blueberry")
    val myAlmondCupcake = Cupcake("Almond")
    val myCheeseCupcake = Cupcake("Cheese")
    val myCaramelCupcake = Cupcake("Caramel")
}*/

/*
class Cupcake(val flavour: String) {

    // En Kotlin el comportamiento de una clase se define como metodos
    fun eat(): String {
        return "nom, nom, nom... delicious $flavour cupcake"
    }
}

fun main(args: Array<String>) {
    val myBlueberryCupcake = Cupcake("Blueberry")
    // Aqui el metodo eat() regresa un valor String con la frase nom, nom, nom... delicious Blueberry cupcake
    println(myBlueberryCupcake.eat())
}*/