# Funtional-kotlin

En este repositorio se aloja el código del libro de Kotlin que estoy leyendo, dicho código se encuentra separado por capítulos tal y como viene en el libro.

 * [Capítulo 01](https://gitlab.com/miguel.gonzalez1/funtional-kotlin/tree/master/src/chapter01)
 * [Capítulo 02](https://gitlab.com/miguel.gonzalez1/funtional-kotlin/tree/master/src/chapter02)
 * [Capítulo 03](https://gitlab.com/miguel.gonzalez1/funtional-kotlin/tree/master/src/chapter03)
 * [Capítulo 04](https://gitlab.com/miguel.gonzalez1/funtional-kotlin/tree/master/src/chapter04)
 * [Capítulo 05](https://gitlab.com/miguel.gonzalez1/funtional-kotlin/tree/master/src/chapter05)
 * [Capítulo 06](https://gitlab.com/miguel.gonzalez1/funtional-kotlin/tree/master/src/chapter06)

El proyecto fué creado en Intellij IDEA y puede ser clonado directamente.

[LICENSE](https://gitlab.com/miguel.gonzalez1/funtional-kotlin/blob/master/LICENSE)